* #### Clase V. Python: Visualización de datos
  
* #### Requisitos
    *  Instalar matplotlib y seaborn usando *pip*. Para eso, abrir una terminal de Linux o MacOS e instalar con el comando *pip install matplotlib seaborn*. La instalación en Windows es ligeramente distinta; pueden seguir [este tutorial](https://datatofish.com/install-package-python-using-pip/).
    *  Si lo anterior no funcionó, descargar [matplotlib](https://files.pythonhosted.org/packages/12/d1/7b12cd79c791348cb0c78ce6e7d16bd72992f13c9f1e8e43d2725a6d8adf/matplotlib-3.1.1.tar.gz). Luego abrir una terminal de Linux o MacOS, dirigirse a la carpeta de descarga e instalar con el comando *pip install <archivo>*. Además, clonar seaborn con `git clone https://github.com/mwaskom/seaborn.git`. Luego abrir una terminal de Linux o MacOS, ingresar a la carpeta clonada e instalar con el comando *pip install <archivo>*.
  
* #### Contenidos
    *  Conceptos adicionales
       * [Cargar datos en Google Colab](https://towardsdatascience.com/3-ways-to-load-csv-files-into-colab-7c14fcbdcb92)
    *  Trabajando con gráficos: matplotlib y seaborn
       * [Google Colab notebook de clase](https://colab.research.google.com/drive/1A1T_0oetLtoHmOkB1hw0xo_lX3Phhmm4)
       * [Matplotlib básico](https://towardsdatascience.com/matplotlib-tutorial-learn-basics-of-pythons-powerful-plotting-library-b5d1b8f67596)
    *  **Ejercicios** :arrow_down:
    *  Soluciones
       * [soluciones_ ejercicio_graficos_1.md ](https://gitlab.com/NPalopoli/python-biocomp/blob/master/Clase_IV/soluciones_%20ejercicio_graficos_1.md)
    
